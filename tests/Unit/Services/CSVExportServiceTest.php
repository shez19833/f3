<?php

namespace Tests\Unit;

use App\Services\CSVExportService;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CSVExportServiceTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Storage::delete('exports.csv');
    }

    public function test_csv_file_is_created_with_right_data()
    {
        $data = [
            [
                'first_name' => 'S',
                'last_name' => 'Sh',
            ],
            [
                'first_name' => 'S1',
                'last_name' => 'Sh2',
            ]
        ];

        $service = new CSVExportService();
        $file = $service->convertToFile($data);

        $fileContents = explode("\n", Storage::get($file));

        $this->assertEquals(['first_name', 'last_name'], explode(",", $fileContents[0]));
        $this->assertEquals(['S', 'Sh'], explode(",", $fileContents[1]));
        $this->assertEquals(['S1', 'Sh2'], explode(",", $fileContents[2]));
    }

    public function tearDown(): void
    {
        Storage::delete('exports.csv');

        parent::tearDown();
    }
}
