<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DownloadsControllerTest extends TestCase
{
    public function test_i_can_get_file_download_if_file_exists()
    {
        $filename = 'test.csv';

        Storage::put($filename, '');

        $this->get(route('download', ['filename' => $filename]))
            ->assertHeader('content-disposition', 'attachment; filename=' . $filename);

        Storage::delete($filename);
    }

    public function test_i_get_404_if_file_doesnt_exist()
    {
        $filename = 'test1.csv';

        $this->get(route('download', ['filename' => $filename]))
            ->assertStatus(404);
    }
}
