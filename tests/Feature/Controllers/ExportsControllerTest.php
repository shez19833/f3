<?php

namespace Tests\Feature;

use App\Services\ExportService;
use Tests\TestCase;

class ExportsControllerTest extends TestCase
{
    public function test_i_can_get_url_to_download_file_if_i_send_data()
    {
        $this->mock(ExportService::class, function ($mock) {
            $mock->shouldReceive('convertToFile')
                ->once()
                ->andReturn('exports2.csv');
        });

        $data = [
            [
                'first_name' => 'S',
                'last_name' => 'Sh',
            ],
            [
                'first_name' => 'S1',
                'last_name' => 'Sh2',
            ]
        ];

        $this->patch(route('export'), $data)
            ->assertJson([
                'url' => route('download', ['filename' => 'exports2.csv'])
            ]);
    }

    public function test_i_cant_get_url_to_download_file_if_i_no_data_sent()
    {
        $this->patch(route('export'))
            ->assertJsonMissing([
                'url' => route('download', ['filename' => 'exports2.csv'])
            ]);
    }
}
