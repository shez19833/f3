<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class CSVExportService implements ExportService
{
    private $filename = 'exports.csv';

    public function convertToFile(array $data) : string
    {
        // TODO would probably randomise the filename if multiple users were
        // using this
        Storage::prepend($this->filename, implode(",", array_keys($data[0])));

        $csv = implode("\n", array_map(function($item) {
            return implode(",", $item);
        }, $data));

        Storage::append($this->filename, $csv);

        return $this->filename;
    }
}
