<?php

namespace App\Services;

interface ExportService
{
    public function convertToFile(array $data) : string;
}
