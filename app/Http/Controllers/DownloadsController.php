<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class DownloadsController extends Controller
{
    public function __invoke($filename)
    {
        // TODO normally should be in form request
        if (!file_exists(Storage::path($filename))) {
            abort(404);
        }

        return response()
            ->download(Storage::path($filename), $filename)
            ->deleteFileAfterSend();
    }
}
