<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ExportService;
use Illuminate\Http\Request;

class ExportsController extends Controller
{
    /**
     * Converts the user input into a CSV file and streams the file back to the user
     */
    public function __invoke(Request $request, ExportService $exportService)
    {
        // TODO normally should be in form request for form validation
        if (!$request->all()) {
            return response()->json(['error' => [
                'message' => 'Data missing'
            ]]);
        }

        $file = $exportService->convertToFile($request->all());

        return response()->json([
            'url' => route('download', ['filename' => $file])
        ]);
    }
}
